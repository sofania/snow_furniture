import 'package:bottom_navigation/appBar.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:flutter/material.dart';

import 'models/viewModel.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _baru()),
    );
  }

  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    getDataUser();
    super.initState();
  }

  Widget _baru() {
    return ListView.builder(
        itemCount: dataUser.length,
        itemBuilder: (context, i) {
          return new Container(
              color: Warna.grey,
              child: Card(
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Image.network(
                        dataUser[i]['gambar'],
                        // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                        fit: BoxFit.cover,
                        width: 100,
                        height: 100,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      height: 100,
                      margin: EdgeInsets.only(left: 8),
                      child: Column(
                        children: [
                          Text(
                            dataUser[i]['nama'],
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.black87,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(20),
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.ac_unit_rounded,
                                color: Colors.lightBlueAccent,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Snow Furniture",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Warna.biruMuda,
                                    backgroundColor: Colors.transparent),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomRight,
                      padding: EdgeInsets.only(left: 20, bottom: 10),
                      height: 100,
                      child: Text(
                        dataUser[i]['harga'],
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}
