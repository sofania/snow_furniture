import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({this.nama, this.harga, this.gambar});

  String nama;
  String harga, gambar;

  Map toJson() => {
        "nama": nama,
        "harga": harga,
        "gambar": gambar,
      };
}
