import 'package:bottom_navigation/appBar.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:bottom_navigation/models/contact.dart';
import 'package:bottom_navigation/ui/entryform.dart';
import 'package:bottom_navigation/ui/viewDaftarProduk.dart';
import 'package:flutter/material.dart';

import 'models/jsonModel.dart';
import 'models/viewModel.dart';

class Produk extends StatefulWidget {
  @override
  _ProdukState createState() => _ProdukState();
}

class _ProdukState extends State<Produk> {
  final TextEditingController _nama = new TextEditingController();
  final TextEditingController _harga = new TextEditingController();
  final TextEditingController _gambar = new TextEditingController();
  Contact contact;
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _form()),
    );
  }

  Widget _form() {
    return new Column(
      children: <Widget>[
        new ListTile(
          leading: const Icon(Icons.paste_rounded),
          title: new TextField(
            controller: _nama,
            decoration: new InputDecoration(
              hintText: "Nama Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.money),
          title: new TextField(
            controller: _harga,
            decoration: new InputDecoration(
              hintText: "Harga Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.image),
          title: new TextField(
            controller: _gambar,
            decoration: new InputDecoration(
              hintText: "Gambar Url produk",
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        new InkWell(
          onTap: () async {
            UserpostModel commRequest = UserpostModel();
            commRequest.nama = _nama.text;
            commRequest.harga = _harga.text;
            commRequest.gambar = _gambar.text;

            UserViewModel()
                .postUser(userpostModelToJson(commRequest))
                .then((value) => print('success'));
          },
          child: Container(
            width: 200,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Warna.orenMuda, Warna.merahKeterangan])),
            child: Text(
              'Tambah Produk',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
