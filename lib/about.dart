import 'package:bottom_navigation/appBar.dart';
import 'package:bottom_navigation/colorPick.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      body: Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.bottomCenter,
          color: Warna.grey,
          child: Column(
            children: <Widget>[
              Text(
                "Copyright S. Galuh Hamidah, 18282018",
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  "Snow Furniture",
                  style: TextStyle(color: Colors.black, fontSize: 17),
                ),
              )
            ],
          )),
    ));
  }
}
